<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Lovacki restoran</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <link rel="stylesheet" href="build/main.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- start:responsive buttons -->
        <div class="resp-buttons">
            <div id="js-resp-menu-toggle" class="resp-menu-icon" >
                <span class="resp-menu-icon__line resp-menu-icon__line--top"></span>
                <span class="resp-menu-icon__line resp-menu-icon__line--middle"></span>
                <span class="resp-menu-icon__line resp-menu-icon__line--bottom"></span>
            </div>
        </div>
        <!-- end:responsive buttons -->
        <header class="header">
            <div class="wrapper wrapper--large">
                <a href="index.php" class="header__logo">
                    <!-- logo -->
                    <img src="resources/img/logo.png" alt="logo" title="Logo" />
                </a>
                <a href="narudzba.php" class="header__cta button button--featured">
                    <span>NARUCI</span> 
                </a>
                <ul class="header__menu">
                    <li>
                        <a href="index.php">Pocetna</a>
                    </li>
                    <li>
                        <a href="o-nama.php">O nama</a>
                    </li>
                    <li>
                        <a href="jelovnik.php">Jelovnik</a>
                    </li>
                    <li>
                        <a href="kontakt.php">Kontakt</a>
                </ul>
            </div>
        </header>