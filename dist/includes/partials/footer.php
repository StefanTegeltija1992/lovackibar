        <!-- start: Footer -->
        <footer class="footer layout">
            <div class="wrapper">
                <ul class="footer__menu">
                    <li>
                        <a href="index.php">Pocetna</a>
                    </li>
                    <li>
                        <a href="o-nama.php">O nama</a>
                    </li>
                    <li>
                        <a href="jelovnik.php">Jelovnik</a>
                    </li>
                    <li>
                        <a href="kontakt.php">Kontakt</a>
                    </li>
                </ul>
                <ul class="footer__social">
                    <li>
                        <a href="https://www.facebook.com/lovackirestoranbanjaluka/" target="_blank">
                            <!-- facebook -->
                            <img src="resources/img/facebook.png" alt="Facebook">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <!-- twitter -->
                            <img src="resources/img/twitter.png" alt="Twitter">
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
        <!-- end: Footer -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="resources/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="resources/js/plugins.js"></script>
        <script src="resources/js/main.js"></script>

        <script src="https://rawgit.com/jonathantneal/svg4everybody/master/dist/svg4everybody.js"></script>
    </body>
</html>
