	// Menu open
	$('#js-resp-menu-toggle').on("click", function () {
		$('body').toggleClass('menu-open');
	});
	
	// Heeader CTA Hover 
	var btn = document.querySelector('.header__cta')
	btn.onmousemove = function (e) {
		var x = e.pageX - btn.offsetLeft - btn.offsetParent.offsetLeft
		var y = e.pageY - btn.offsetTop - btn.offsetParent.offsetTop
		btn.style.setProperty('--x', x + 'px')
		btn.style.setProperty('--y', y + 'px')
	};

