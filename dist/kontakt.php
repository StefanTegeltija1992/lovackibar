<?php include 'includes/partials/header.php';?>

<section class="contact__content__block">
	<h1 class="contact__content__block__heading">Kontakt</h1>
	<p class="contact__content__block__text">Radno vrijeme: 10:00 - 22:00</p>
	<p class="contact__content__block__text">Adresa: Slatinska 37, Banja Luka</p>
	<p class="contact__content__block__text">Tel: <a href="tel:051312243">051 312-243</a></p>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3365.7524750023604!2d17.218793948858423!3d44.816487810065!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475e03d70aa8409d%3A0xe3b27c7e7e881faf!2sLova%C4%8Dki+restoran!5e0!3m2!1sen!2sba!4v1522656384989" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<!-- start: Narudzba -->
<section class="contact">
    <div class="wrapper wrapper--small">
        <h2 class="contact__title">Narucite</h2>
        <div class="contact__form__kontakt-page">
            <form action="https://formspree.io/posao.infomedia@gmail.com" method="POST">
                <input placeholder="Full Name" class="contact__form__field" type="text" name="Full Name" autocomplete="name" required>
                <input placeholder="Email Address" class="contact__form__field" type="email" name="Email Address" autocomplete="email" required>
                <input placeholder="Adresa za dostavu narudzbe" class="contact__form__field" type="text" autocomplete="organization" name="Adresa za dostavu narudzbe">
                <textarea placeholder="Detalji narudzbe" class="contact__form__field contact__form__field--textarea" name="Detalji narudzbe"></textarea>
                <input class="button button--primary" type="submit" value="Send">
            </form>
        </div>
    </div>
</section>
<!-- end: Narudzba -->

<?php include 'includes/partials/footer.php';?>