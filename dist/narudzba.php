<?php include 'includes/partials/header.php';?>

<!-- start: Narudzba -->
<section class="contact">
    <div class="wrapper wrapper--small">
        <div class="contact__form">
            <form action="https://formspree.io/posao.infomedia@gmail.com" method="POST">
                <input placeholder="Full Name" class="contact__form__field" type="text" name="Full Name" autocomplete="name" required>
                <input placeholder="Email Address" class="contact__form__field" type="email" name="Email Address" autocomplete="email" required>
                <input placeholder="Adresa za dostavu narudzbe" class="contact__form__field" type="text" autocomplete="organization" name="Adresa za dostavu narudzbe">
                <textarea placeholder="Detalji narudzbe" class="contact__form__field contact__form__field--textarea" name="Detalji narudzbe"></textarea>
                <input class="button button--primary" type="submit" value="Send">
            </form>
        </div>
    </div>
</section>
<!-- end: Narudzba -->

<?php include 'includes/partials/footer.php';?>