let gulp            = require('gulp');
let sass            = require('gulp-sass');
let sourcemaps      = require('gulp-sourcemaps');
let autoprefixer    = require('gulp-autoprefixer');
let cleanCSS        = require('gulp-clean-css');
let svgSprite       = require('gulp-svg-sprite');
let rename          = require('gulp-rename');
let browserSync         = require('browser-sync').create();

let input   = './resources/scss/**/*.scss';
let output  = './build';

let sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

let autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

// Browsersync
var bsync = function (proxy) {
    if (proxy) {
        browserSync.init({
            proxy: proxy
        });
    }
    else {
        browserSync.init({
            server: {
                baseDir: './'
            },
            online: true
        });
    }
};

gulp.task('sass', function () {
    return gulp
    // Find all `.scss` files from the `stylesheets/` folder
    .src(input)
    // Run Sass on those files
    .pipe(sass(sassOptions).on('error', sass.logError))
    // Sourcemaps for better view of styles
    .pipe(sourcemaps.write())
    // Let autoprefixer add necessary vendor prefixes
    .pipe(autoprefixer(autoprefixerOptions))
    // Minify the css
    .pipe(cleanCSS({compatibility: 'ie8'}))
    // Write the resulting CSS in the output folder
    .pipe(gulp.dest(output))
    // Rename default css for cache busting
    .pipe(rename("./build/main.css"))
    .pipe(gulp.dest("./"))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('watch', function() {
  bsync('http://localhost/lovackibar/');
  return gulp
    // Watch the input folder for change,
    // and run `sass` task when something happens
    .watch(input, ['sass'])
    // When there is a change,
    // log a message in the console
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

//SVG Sprite Builder
var svgBuildOptions = {
    mode: {
        symbol: { // symbol mode to build the SVG
            dest: 'build/svg/', // destination folder
            sprite: 'sprite.svg', //sprite name
            example: true // Build sample page
        }
    },
    svg: {
        xmlDeclaration: false, // strip out the XML attribute
        doctypeDeclaration: false // don't include the !DOCTYPE declaration
    }
};


// Build SVG Sprite
gulp.task('build-svg-sprite', function() {
    return gulp.src( 'resources/svg/*.svg' )
        .pipe(svgSprite(svgBuildOptions))
        .on('error',console.log.bind(console))
        .pipe(gulp.dest('.'));
});

gulp.task('default', ['build-svg-sprite','sass', 'watch',]);

gulp.task('build', ['build-svg-sprite','sass', ]);